from django.conf.urls import patterns, include, url
from django.contrib import admin


from Yet_Another_Auction_Site_App.views_default import *
from Yet_Another_Auction_Site_App.views_authenticate import *
from Yet_Another_Auction_Site_App.views_users import *
from Yet_Another_Auction_Site_App.views_auctions import *
from Yet_Another_Auction_Site_App.rest_views import *


urlpatterns = patterns('',
                       url(r'^$', home, name='home'),

                       # registration, login, logout - views_authenticate
                       url(r'^registration/$', registration, name="registration"),
                       url(r'^login/$', login_view, name="login"),
                       url(r'^logout/$', logout_view, name="logout"),
                       url(r'^profile/$', edit_profile, name="edit_profile"),

                       url(r'^create_auction/$', create_auction, name="create_auction"),
                       url(r'^view/(?P<id_auction>\d+)$', view_auction, name="view_auction"),
                       url(r'^search/$', search_auction, name="search_auction"),
                       url(r'^ban/(?P<id_auction>\d+)$', ban_auction, name="ban_auction"),
                       url(r'^edit/(?P<id_auction>\d+)$', edit_auction, name="edit_auction"),
                       url(r'^bid/(?P<id_auction>\d+)$', bid_auction, name="bid_auction"),


                       url(r'^translate/$', translate, name="translate"),

                       url(r'^api/v1/auctions/$', auction_list),
                       url(r'^api/v1/search/(?P<search_value>[a-zA-Z0-9_!@#$%^&*()-, ./;"]+)/$', api_search_auction),
                       url(r'^api/v1/bid/(?P<id_auction>[0-9]+)/$', api_bid_auction),

                       url(r'^populate_db/$', populate_db, name="populate_db"),
                       url(r'^admin/', include(admin.site.urls)),
)
