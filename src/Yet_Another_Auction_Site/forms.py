__author__ = 'ann'


from django.contrib.auth.models import User
from Yet_Another_Auction_Site_App.models import Auction

from datetime import datetime
from django.utils.timezone import utc
import re
from django.core.validators import MinValueValidator, validate_slug, validate_email

from django import forms
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm


DATE_FORMAT = '%Y-%m-%d %H:%M:%S'


class UserRegistrationForm(UserCreationForm):
    error_css_class = 'error'
    required_css_class = 'required'
    first_name = forms.CharField(required=True, help_text='Required')
    last_name = forms.CharField(required=True, help_text='Required')
    email = forms.EmailField(required=True,  help_text='Required', validators=[validate_email])
    username = forms.CharField(required=True, help_text='Required', validators=[validate_slug])

    class Meta:
        model = User
        fields = ("first_name", "last_name", "email", "username", "password1", "password2", )


class UserEditForm(PasswordChangeForm):
    error_css_class = 'error'
    required_css_class = 'required'
    email = forms.EmailField(required=False, validators=[validate_email])

    def save(self):
        user = super(UserEditForm, self).save()
        email = self.cleaned_data["email"]
        if not email == "":
            user.email = self.cleaned_data["email"]
        user.save()
        return user



class CreateAuctionForm(forms.Form):
    title_auction = forms.CharField(required=True, help_text='Required')
    description = forms.CharField(widget=forms.Textarea(), required=True)
    min_price = forms.FloatField(required=True,  help_text='Required', validators=[MinValueValidator(0)])
    end_date = forms.CharField(help_text='The minimum duration is 72 hours')
    date_format = forms.CharField(help_text='Default: Y-m-d H:M:S. If you want to use another type of date, please'
                                            ' specify it clearly, otherwise you can leave it blank', required=False)
    #id_status = forms.ModelChoiceField(queryset=Status.objects.all())

    class Meta:
        model = Auction

    def save(self, request):
        auction = Auction()
        auction.title_auction = self.cleaned_data["title_auction"]
        auction.description = self.cleaned_data["description"]
        auction.min_price = self.cleaned_data["min_price"]
        auction.end_date = self.cleaned_data['end_date']
        user_object = User.objects.get(id=request.user.id)
        auction.user = user_object
        auction.save()
        print(auction.end_date)
        return {"type": "success", "message": 'The auction was created.', 'object': auction}

    def verify(self):
        date_format = self.cleaned_data['date_format']
        date = self.cleaned_data['end_date']

        # default data format was used
        if date_format == "":
            date_format = DATE_FORMAT
        else:
            # lets create an input format in order to be able to convert it => 'Y-m-d' => %Y-%m-%d
            date_format_temp = ""
            pattern = r"[a-zA-Z]{1,4}"
            iterator = re.finditer(pattern, date_format)
            for match in iterator:
                date_format_temp += "%" + date_format[match.start():(match.end()+1)]
            date_format = date_format_temp
        try:
            self.cleaned_data['end_date'] = end_date = datetime.strptime(date, date_format).replace(tzinfo=utc)
        except:
            return {"type": "error", "message": "Please specify the right date format"}

        # if the data was specified correctly then verify if the deadline is later than in 72 hours
        date_diff = end_date - datetime.utcnow().replace(tzinfo=utc)
        # 72 hours = 259200 sec
        if not date_diff.total_seconds() >= 259200:
            return {"type": "error", "message": "The deadline is less than 72 hours"}

        return {"type": "success", "message": 'The auction can be created.', 'form': self}