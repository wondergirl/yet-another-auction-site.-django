__author__ = 'ann'


from django.utils.html import escape
from django.utils import translation
from django.conf import settings


def html_to_content(h):
    h = h.strip()
    return escape(h)


def is_mine(id, request):
    return request.user.id == id


def check_lang(request):
    try:
        if not request.session["lang"]:
            request.session["lang"] = settings.DEFAULT_LANG
            translation.activate(request.session["lang"])
        else:
            translation.activate(request.session["lang"])
    except KeyError:
        request.session["lang"] = settings.DEFAULT_LANG
        translation.activate(request.session["lang"])
    return True