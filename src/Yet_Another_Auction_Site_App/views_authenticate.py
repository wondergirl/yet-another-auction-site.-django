__author__ = 'ann'

from django.http import HttpResponseRedirect
from Yet_Another_Auction_Site.forms import UserRegistrationForm
from django.shortcuts import render
from django.contrib import messages
from django.core.urlresolvers import reverse
from Yet_Another_Auction_Site_App.views_helpers import html_to_content
from django.contrib.auth import authenticate, login, logout


def registration(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.INFO, 'The user was registered. Please login')
            return HttpResponseRedirect(reverse("login"))
    else:
        form = UserRegistrationForm()

    return render(request, "authenticate/base_register.html", {'form': form, 'page_name': "Registration"})


def login_view(request):
    if request.method == "POST":
        username = html_to_content(request.POST.get('username', ''))
        password = html_to_content(request.POST.get('password', ''))
        next_to = request.GET.get('next', reverse('home'))

        user = authenticate(username=username, password=password)

        if user is None:
            messages.add_message(request, messages.INFO, "Wrong username or password")
            return render(request, "authenticate/login_form.html",
                          {'page_name': "Login",
                           'username': username})
        if not user.is_active:
            messages.add_message(request, messages.INFO, "This user is not active. Please contact administrator")
            return render(request, "authenticate/login_form.html",
                          {'page_name': "Login"})
        login(request, user)
        return HttpResponseRedirect(next_to)

    else:
        return render(request, "authenticate/login_form.html", {'page_name': "Login"})


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('home'))