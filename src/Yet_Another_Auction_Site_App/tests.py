from django.core import mail
from django.test import TestCase

from Yet_Another_Auction_Site_App.models import Auction, Bids
from django.contrib.auth.models import User

from django.core.urlresolvers import reverse
import random


class CreateAuctionTesting(TestCase):
    fixtures = ['data.json']

    def setUp(self):
        self.deadline = '29-11-2014'

        self.auction = Auction()
        self.username = "test"
        self.email = "ana@utu.fi"
        self.password = "test"
        self.id_user = 1
        self.test_user = User.objects.create_user(self.username, self.email, self.password)
        login = self.client.login(username=self.username, password=self.password)
        self.assertEqual(login, True)

    # page access tests
    def test_not_existing_access(self):
        resp = self.client.get('/create_auction/9000000000')
        self.assertEqual(resp.status_code, 404)

    def test_alpha_access(self):
        resp = self.client.get('/create_auction/dsd-ew')
        self.assertEqual(resp.status_code, 404)

    def test_alpha_num_access(self):
        resp = self.client.get('/create_auction/ds32')
        self.assertEqual(resp.status_code, 404)

    def test_create_auction(self):
        # positive testing
        resp = self.client.post('/create_auction/',
                                {'title_auction': 'title', 'description': 'description',
                                 'min_price': 200, 'end_date': self.deadline,
                                 'id_user': self.id_user, 'date_format': "d-m-Y",
                                 'confirm': ''})
        self.assertIn("name=\"confirm\"", resp.content)
        self.assertIn("name=\"cancel\"", resp.content)

        # confirmation page - Canceled
        resp = self.client.post('/create_auction/',
                                {'title_auction': 'title', 'description': 'description',
                                 'min_price': 200, 'end_date': self.deadline,
                                 'id_user': self.id_user,
                                 'cancel': 'No', 'date_format': "d-m-Y",})
        self.assertRedirects(resp, reverse("home"))

        # confirmation page - Confirmed
        resp = self.client.post('/create_auction/',
                                {'title_auction': 'title', 'description': 'description',
                                 'min_price': 200, 'end_date': self.deadline,
                                 'confirm': 'Yes', 'date_format': "d-m-Y"})
        self.assertRedirects(resp, reverse("home"))


        # Negative testing
        # 1 - Data validation
        resp = self.client.post('/create_auction/',
                                {'title_auction': '           <script>alert("Hello!)</script>\'OR 1=1\'',
                                 'min_price': '<?php echo "anna"; ?>', 'description': 'description',
                                 'end_date': 'jjjjjjj4-11-2014',
                                 'id_user': self.id_user, 'date_format': "oooo=d-m-Y"})
        self.assertEqual(resp.status_code, 200)


        # 2 - not all data provided  - no title
        resp = self.client.post('/create_auction/',
                                {'description': 'description',
                                 'min_price': 200, 'end_date': '4-11-2014',
                                 'id_user': self.id_user, 'date_format': "d-m-Y"})
        self.assertEqual(resp.status_code, 200)
        self.assertIn("This field is required", resp.content)

        # 3 - not all data provided  - no description
        resp = self.client.post('/create_auction/',
                                {'title_auction': 'title',
                                 'min_price': 200, 'end_date': '4-11-2014',
                                 'id_user': self.id_user, 'date_format': "d-m-Y"})
        self.assertEqual(resp.status_code, 200)
        self.assertIn("This field is required", resp.content)

        # 4 - not all data provided  - no price
        resp = self.client.post('/create_auction/',
                                {'title_auction': 'title',
                                 'description': "200", 'end_date': '4-11-2014',
                                 'id_user': self.id_user, 'date_format': "d-m-Y"})
        self.assertEqual(resp.status_code, 200)
        self.assertIn("This field is required", resp.content)

        # 4 - not all data provided  - no end date
        resp = self.client.post('/create_auction/',
                                {'title_auction': 'title',
                                 'min_price': 200, 'description': '4-11-2014',
                                 'id_user': self.id_user, 'date_format': "d-m-Y"})
        self.assertEqual(resp.status_code, 200)
        self.assertIn("This field is required", resp.content)

        # 4 - not data provided at all
        resp = self.client.post('/create_auction/',
                                {'title_auction': '',
                                 'min_price': '', 'description': '',
                                 'id_user': '', 'date_format': "", 'end_date': ''})
        self.assertEqual(resp.status_code, 200)
        self.assertIn("This field is required", resp.content)

    def test_email(self):
        resp = self.client.post('/create_auction/',
                                    {'title_auction': 'title', 'description': 'description',
                                     'min_price': 200, 'end_date': self.deadline,
                                     'id_user': self.id_user, 'date_format': "d-m-Y",
                                     'confirm': 'Yes'})
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'The Auction was launched!')

    def test_deadline_date(self):
        test_date = '1-11-2014'
        resp = self.client.post('/create_auction/',
                                    {'title_auction': 'title', 'description': 'description',
                                     'min_price': 200, 'end_date': test_date,
                                     'id_user': self.id_user, 'date_format': "d-m-Y"})
        self.assertEqual(resp.status_code, 200)
        self.assertIn("The deadline is less than 72 hours", resp.content)

    def test_date_format(self):
        resp = self.client.post('/create_auction/',
                                    {'title_auction': 'title', 'description': 'description',
                                     'min_price': 200, 'end_date': '1-11-2014',
                                     'id_user': self.id_user, 'date_format': "MM-m-YYYY H:S"})
        self.assertEqual(resp.status_code, 200)
        self.assertIn("Please specify the right date format", resp.content)

    def test_auth_access(self):
        resp = self.client.get('/create_auction/')
        self.assertEqual(resp.status_code, 200)
        self.assertIn("form", resp.content)

    def test_not_auth_access(self):
        self.client.logout()
        resp = self.client.get('/create_auction/')
        self.assertEqual(resp.status_code, 302)
        self.assertRedirects(resp, '/login/?next=/create_auction/')


class BiddingTesting(TestCase):
    fixtures = ['data.json']

    def setUp(self):
        self.existing_auction = 47
        self.bid_price = 2000
        self.id_user = 1
        self.version = 2
        self.auction = Auction()
        self.username = "test"
        self.email = "ana@utu.fi"
        self.password = "test"
        self.test_user = User.objects.create_user(self.username, self.email, self.password)
        login = self.client.login(username=self.username, password=self.password)
        self.assertEqual(login, True)

    # page access tests
    def test_not_existing_access(self):
        resp = self.client.get('/bid/9000000000')
        self.assertEqual(resp.status_code, 302)

    def test_alpha_access(self):
        resp = self.client.get('/bid/dsd-ew')
        self.assertEqual(resp.status_code, 404)

    def test_alpha_num_access(self):
        resp = self.client.get('/bid/ds32')
        self.assertEqual(resp.status_code, 404)

    def test_bid_auction(self):
        session = self.client.session
        session['auction_' + str(self.existing_auction) +'_v'] = 1
        session["auction_" + str(self.existing_auction) + "_id_winner"] = 2
        session.save()

        #bid = Bids.objects.get(auction_id=self.existing_auction)
        price = Auction.objects.get(id_auction=self.existing_auction)
        bid = Bids.get_winner(self.existing_auction)

        bid_price = random.randrange(100000)
        resp = self.client.post('/bid/' + str(self.existing_auction),
                                {'bid_price': bid_price})
        print ("Bid price: " + str(bid_price) + "Price: " + str(price.min_price) + "Winning bid: " + str(bid.price))

        if bid.user_id == self.id_user:
            self.assertIn("You cannot bid this auction because it you already winning it", resp.content)

        if bid.auction.user == self.id_user:
            self.assertIn("You cannot bid this auction because it is yours", resp.content)

        if bid.auction.version == self.version:
            self.assertIn("You cannot bid this auction because the description has changed", resp.content)

        session['auction_' + str(self.existing_auction) +'_v'] = 1
        session.save()

        if float(bid_price) < 0.01:
            self.assertIn("class=\"error\"", resp.content)
        elif float(bid_price) <= float(bid.price):
            self.assertIn("class=\"error\"", resp.content)
        elif float(bid_price) <= float(price.min_price):
            self.assertIn("class=\"error\"", resp.content)

        if float(bid_price) > float(bid.price) and float(bid_price) > 0 and float(bid.price) > 0:
            self.assertIn("You placed a bid successfully", resp.content)
            self.assertEqual(len(mail.outbox), 1)
            self.assertEqual(mail.outbox[0].subject, 'The Auction was bidded!')


        session["auction_" + str(self.existing_auction) + "_id_winner"] = 1
        session.save()

        if bid.user_id == self.id_user:
            self.assertIn("You cannot bid this auction because it you already winning it", resp.content)

    def test_validate_bid_field(self):
        session = self.client.session
        session['auction_' + str(self.existing_auction) +'_v'] = 1
        session["auction_" + str(self.existing_auction) + "_id_winner"] = 2
        session.save()

        # Negative testing
        # 1.1 - Data validation
        resp = self.client.post('/bid/' + str(self.existing_auction),
                                {'bid_price': '            <script>alert("Hello!)</script>\'OR 1=1\''})
        self.assertEqual(resp.status_code, 200)
        self.assertIn("Introduce the right price", resp.content)

        # 1.2 - Data validation
        resp = self.client.post('/bid/' + str(self.existing_auction),
                                {'bid_price': '2222,2222222'})
        self.assertEqual(resp.status_code, 200)
        self.assertIn("Introduce the right price", resp.content)

        # 1.3 - Data validation
        resp = self.client.post('/bid/' + str(self.existing_auction),
                                {'bid_price': 'sdsdsdsd sd dsdsds'})
        self.assertEqual(resp.status_code, 200)
        self.assertIn("Introduce the right price", resp.content)

         # 1.4 - Data validation
        resp = self.client.post('/bid/' + str(self.existing_auction),
                                {'bid_price': -233})
        self.assertEqual(resp.status_code, 200)
        self.assertIn("The minimum bid is", resp.content)

        # 2 - not all data provided
        resp = self.client.post('/bid/' + str(self.existing_auction),
                                {'bid_price': ''})
        self.assertEqual(resp.status_code, 200)
        self.assertIn("Introduce the right price", resp.content)

    def test_auth_access(self):
        resp = self.client.get('/bid/' + str(self.existing_auction))
        self.assertEqual(resp.status_code, 200)
        self.assertIn("form", resp.content)

    def test_not_auth_access(self):
        self.client.logout()
        resp = self.client.get('/bid/' + str(self.existing_auction))
        self.assertEqual(resp.status_code, 302)
        self.assertRedirects(resp, '/login/?next=/bid/' + str(self.existing_auction))


class ConcurrencyTesting(TestCase):
    fixtures = ['data.json']

    def setUp(self):
        self.existing_auction = 47
        self.bid_price = 2000
        self.id_user = 1
        self.version = 2
        self.auction = Auction()
        self.username = "test"
        self.email = "ana@utu.fi"
        self.password = "test"
        self.test_user = User.objects.create_user(self.username, self.email, self.password)
        login = self.client.login(username=self.username, password=self.password)
        self.assertEqual(login, True)

    def test_bid_auction(self):
        session = self.client.session
        session['auction_' + str(self.existing_auction) +'_v'] = 1
        session["auction_" + str(self.existing_auction) + "_id_winner"] = 2
        session.save()


        bid = Bids.get_winner(self.existing_auction)
        winner = Bids.get_winner(self.existing_auction)
        price = Auction.objects.get(id_auction=self.existing_auction)

        bid_price = random.randrange(100000)
        resp = self.client.post('/bid/' + str(self.existing_auction),
                                {'bid_price': bid_price})
        print ("Bid price: " + str(bid_price) + "Price: " + str(bid.price) + "Winning bid: " + str(bid.price))
        print("Version is session: " + str(self.version) + " Real version: " + str(bid.auction.version))


        if float(bid_price) < 0.01:
            self.assertIn("class=\"error\"", resp.content)
        elif float(bid_price) <= float(bid.price):
            self.assertIn("class=\"error\"", resp.content)
        elif float(bid_price) <= float(price.min_price):
            self.assertIn("class=\"error\"", resp.content)

        if float(bid_price) > float(bid.price) and float(bid_price) > 0 and float(bid.price) > 0:
            # the description has changed
            if bid.auction.version == self.version:
                self.assertIn("You cannot bid this auction because the description has changed", resp.content)
            # the description has not changed
            else:
                self.assertNotIn("You cannot bid this auction because the description has changed", resp.content)

            # the winner has not changed
            if winner.id_bid == session['auction_' + str(self.existing_auction) +'_id_winner']:
                self.assertIn("class=\"info\"", resp.content)
            # the winner has changed
            else:
                self.assertNotIn("class=\"error\"", resp.content)


            self.assertIn("You placed a bid successfully", resp.content)
            self.assertEqual(len(mail.outbox), 1)
            self.assertEqual(mail.outbox[0].subject, 'The Auction was bidded!')

    def test_ban_auction(self):
        session = self.client.session
        session['auction_' + str(self.existing_auction) +'_view_v'] = 1
        session.save()

        resp = self.client.get('/bid/' + str(self.existing_auction))
        auction = Auction.objects.get(id_auction =self.existing_auction)

        print("Version is session: " + str(session['auction_' + str(self.existing_auction) +'_view_v']) +
              " Real version: " + str(auction.version))

        # the description has changed
        if auction.version == self.version:
            self.assertIn("class=\"error\"", resp.content)
        # the description has not changed
        else:
            self.assertNotIn("class=\"info\"", resp.content)





