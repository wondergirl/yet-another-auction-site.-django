__author__ = 'ann'


from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.db.models import Max

class Status(models.Model):
    id_status = models.AutoField(primary_key=True, null=False)
    title = models.CharField(max_length=100, null=False)


class Auction(models.Model):
    id_auction = models.AutoField(primary_key=True, null=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='%(class)s_seller')
    title_auction = models.CharField(max_length=150)
    description = models.TextField()
    min_price = models.FloatField(null=False)
    end_date = models.DateTimeField()
    status = models.ForeignKey(Status, null=False, default=1)
    version = models.FloatField(null=False, default=1)
    id_winner = models.IntegerField(null=False, default=0)

    class Meta:
        ordering = ['end_date']

    def add_auction(self, request, data):
        auction = Auction()
        auction.title_auction = self.cleaned_data["title_auction"]
        auction.description = self.cleaned_data["description"]
        auction.min_price = self.cleaned_data["min_price"]
        auction.end_date = self.cleaned_data["end_date"]
        user = User.objects.get(id=request.user.id)
        auction.user = user
        auction.save()
        return auction


class Bids(models.Model):
    id_bid = models.AutoField(primary_key=True, null=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=False)
    auction = models.ForeignKey(Auction, null=False)
    date = models.DateTimeField(null=False, auto_now=True)
    price = models.FloatField(null=False)

    class Meta:
        ordering = ['date']

    def place_bid(self, data):
        bid = Bids()
        auction = Auction.objects.get(id_auction=data.get('id_auction'))
        user = User.objects.get(id=data.get('id_user'))
        bid.auction = auction
        bid.price = data.get('bid_price')
        bid.user = user
        bid.save()
        return True


    @staticmethod
    def get_winner(id_auction):
        winning_bid = Bids.objects.filter(auction_id=id_auction).all().aggregate(Max('price'))['price__max']
        if not winning_bid:
            return None

        try:
            return Bids.objects.get(auction_id=id_auction, price=winning_bid)
        except Bids.DoesNotExist:
            return None


class Winners(models.Model):
    auction = models.ForeignKey(Auction, null=False)
    bid = models.ForeignKey(Bids, null=False)

    class Meta:
        unique_together = ("auction", "bid")



