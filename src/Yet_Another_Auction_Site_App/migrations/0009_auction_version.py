# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Yet_Another_Auction_Site_App', '0008_auto_20141101_2028'),
    ]

    operations = [
        migrations.AddField(
            model_name='auction',
            name='version',
            field=models.FloatField(default=1),
            preserve_default=True,
        ),
    ]
