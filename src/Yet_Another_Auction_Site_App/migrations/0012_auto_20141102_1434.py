# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Yet_Another_Auction_Site_App', '0011_auto_20141102_1433'),
    ]

    operations = [
        migrations.RenameField(
            model_name='auction',
            old_name='winner',
            new_name='id_winner',
        ),
    ]
