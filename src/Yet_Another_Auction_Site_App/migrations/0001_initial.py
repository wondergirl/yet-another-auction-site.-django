# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Auction',
            fields=[
                ('id_auction', models.AutoField(serialize=False, primary_key=True)),
                ('title_auction', models.CharField(max_length=150)),
                ('description', models.TextField()),
                ('min_price', models.FloatField()),
                ('end_date', models.DateTimeField()),
            ],
            options={
                'ordering': ['end_date'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Bids',
            fields=[
                ('id_bid', models.AutoField(serialize=False, primary_key=True)),
                ('date', models.DateTimeField(auto_now=True)),
                ('price', models.FloatField()),
                ('id_auction', models.ForeignKey(to='Yet_Another_Auction_Site_App.Auction')),
                ('id_user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id_status', models.AutoField(serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='auction',
            name='id_status',
            field=models.ForeignKey(default=1, to='Yet_Another_Auction_Site_App.Status'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='auction',
            name='id_user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]
