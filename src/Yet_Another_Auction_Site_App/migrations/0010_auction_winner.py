# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('Yet_Another_Auction_Site_App', '0009_auction_version'),
    ]

    operations = [
        migrations.AddField(
            model_name='auction',
            name='winner',
            field=models.ForeignKey(related_name='auction_winner', default=0, to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]
