# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Yet_Another_Auction_Site_App', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='bids',
            options={'ordering': ['date']},
        ),
    ]
