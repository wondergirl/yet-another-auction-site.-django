from django.contrib.auth import update_session_auth_hash

__author__ = 'ann'


from django.contrib.auth.models import User
from Yet_Another_Auction_Site.forms import UserEditForm

from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse


@login_required
def edit_profile(request):
    id_user = request.user.id
    user = User.objects.get(id=id_user)
    if not user.is_active:
        messages.add_message(request, messages.ERROR, 'You cannot edit your profile because your account is not active')
        return HttpResponseRedirect(reverse("home"))

    if request.method == 'POST':
        form = UserEditForm(user=request.user, data=request.POST)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            messages.add_message(request, messages.INFO, 'Your profile info has been changed')
    else:
        form = UserEditForm(user=request.user)

    return render(request, "authenticate/base_edit_account.html", {'form': form, 'page_name': "Edit account data"})